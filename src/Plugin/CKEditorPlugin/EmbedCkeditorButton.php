<?php

namespace Drupal\ckeditor_instagram_embed\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "ckeditor_instagram_embed" plugin.
 *
 * NOTE: The plugin ID ('id' key) corresponds to the CKEditor plugin name.
 * It is the first argument of the CKEDITOR.plugins.add() function in the
 * plugin.js file.
 *
 * @CKEditorPlugin(
 *   id = "ckeditor_instagram_embed",
 *   label = @Translation("Embed ckeditor button"),
 *   module = "ckeditor_instagram_embed"
 * )
 */
class EmbedCkeditorButton extends CKEditorPluginBase {

  /**
   * Current module path
   *
   * @var string
   */
  protected $module_path;

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->module_path = \Drupal::service('module_handler')->getModule('ckeditor_instagram_embed')->getPath();
  }

  /**
   * {@inheritdoc}
   *
   * NOTE: The keys of the returned array corresponds to the CKEditor button
   * names. They are the first argument of the editor.ui.addButton() or
   * editor.ui.addRichCombo() functions in the plugin.js file.
   */
  public function getButtons() {
    return [
      'ckeditor_instagram_embed' => [
        'label' => t('Embed ckeditor button'),
        'image' => $this->module_path . '/js/plugin/ckeditor_instagram_embed/icons/icon.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {}

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->module_path . '/js/plugin/ckeditor_instagram_embed/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
