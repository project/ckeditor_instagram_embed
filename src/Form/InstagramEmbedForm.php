<?php

namespace Drupal\ckeditor_instagram_embed\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Entity\Editor;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandler;

/**
 * Provides a setting UI for an embed Instagram post.
 *
 * @see https://www.instagram.com/developer/embedding/#oembed
 */
class InstagramEmbedForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function __construct(ModuleHandler $moduleHandler) {
    $this->module_handler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckeditor_instagram_embed_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Editor $editor = NULL) {
    global $base_url;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';

    $form['#prefix'] = '<div id="editor-oembed-instagram-dialog">';
    $form['#suffix'] = '</div>';

    $link = Link::fromTextAndUrl('Embed Button', Url::fromUri('https://developers.facebook.com/docs/instagram/embed-button/'));
    $path = $this->module_handler->getModule('ckeditor_instagram_embed')->getPath();

    $form['flex'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['flex-container']
      ],
      'insert_code' => [
        '#type' => 'textarea',
        '#title' => $this->t('Insert code'),
        '#description' => $this->t('Follow the instructions — @link.', [
          '@link' => Markup::create($link->toString()->__toString())
        ]),
        '#required' => TRUE,
      ],
      'instruction_text' => [
        '#type' => 'container',
        '#attributes' => [
          'style' => 'max-height: 500px; overflow:auto;'
        ],
        'template' => [
          '#theme' => "ckeditor_embed_help",
          '#path' => "$base_url/$path/"
        ]
      ]
    ];

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => '555',
    ];

    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#editor-oembed-instagram-dialog', $form));
    }
    else {
      $form_state->setValue('insert_code', $form_state->getValue('insert_code'));
      $response->addCommand(new EditorDialogSave($form_state->getValue('insert_code')));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;

  }


}
