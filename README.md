CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Recommended modules
 * Installation
 * Configuration


INTRODUCTION
------------

You can get a public Instagram photo, video, Reel, or Guide post’s embed HTML code and use it to embed the post in other websites. Embedded posts automatically resize to fit their container, so they will look great on both desktop and mobile websites.

RECOMMENDED MODULES
-------------------

 * No extra module is required.


INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8
   for further information.


CONFIGURATION
-------------

  * No configure is required.


REQUIREMENTS
------------

  * No requirements is required.
