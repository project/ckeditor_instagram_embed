/**
 * Inserting instagram post to the editor
 */
 (function ($, Drupal, drupalSettings, CKEDITOR) {
  'use strict';

  CKEDITOR.plugins.add('ckeditor_instagram_embed', {
    hidpi: true,
    beforeInit: function (editor) {
      editor.addCommand('ckeditor_instagram_embed', {
        modes: {wysiwyg: 1},
        canUndo: true,
        exec: function exec(editor) {
          let existingValues = {};
          let dialogSettings = {
            title: Drupal.t('Embed an Instagram'),
            dialogClass: 'editor-oembed-instagram-dialog'
          };
          var insertContent = function(html) {
            editor.insertHtml(html);
          };
          var saveCallback = function(data) {
            if(data) {
              insertContent(data);
            }
          };
          Drupal.ckeditor.openDialog(editor, Drupal.url('admin/ckeditor_instagram_embed/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);
        }
      });
      editor.ui.addButton('ckeditor_instagram_embed', {
        label: Drupal.t('Instagram Embed Button'),
        command: 'ckeditor_instagram_embed',
        icon: this.path + 'icons/icon.png'
      });
    }
  });
})(jQuery, Drupal, drupalSettings, CKEDITOR);
